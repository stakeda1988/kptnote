new Vue({
  el: "#input",
  data: {
    users: [
      {
        id: 0,
        class: ""
      }
    ],
    user: {
      name: "",
      class: ""
    }
  },
  methods: {
    addKItem: function(e) {
      var user = {
        id: this.user.id,
        name: this.user.name,
        class: this.user.class
      };
      user.id = parseInt(this.users[ this.users.length - 1 ].id) + 1;
      user.class = 'K';
      this.users.push(user);
      console.log("kuser" + user.id);
    },
    addPItem: function(e) {
      var user = {
        id: this.user.id,
        name: this.user.name,
        class: this.user.class
      };
      user.id = parseInt(this.users[ this.users.length - 1 ].id) + 1;
      user.class = 'P';
      this.users.push(user);
      console.log("puser" + user.id);
    },
    addTItem: function(e) {
      var user = {
        id: this.user.id,
        name: this.user.name,
        class: this.user.class
      };
      user.id = parseInt(this.users[ this.users.length - 1 ].id) + 1;
      user.class = 'T';
      this.users.push(user);
      console.log("tuser" + user.id);
    },
    getUserById: function(id){
      var user = _.select(this.users, function(obj){
        return obj.id == id;
      });
      return user[0];
    },
    deleteUserById: function(id){
      this.deleteUser(this.getUserById(id));
    },
    deleteUser: function(user){
      console.log(user.id);
      return this.users.splice(user.id, 1)[0];
    }
  }
});
